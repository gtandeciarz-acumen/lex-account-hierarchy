({
	doInit : function(component, event, helper) {
        var action = component.get("c.getHierarchyData");
        action.setParams({
            accountId: component.get("v.recordId") /* here it sends a null value*/
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set("v.treeItems", response.getReturnValue());
                console.log(component.get("v.recordId"));
                console.log(response.getReturnValue());
            }
            else
                console.log(response);
        });
        $A.enqueueAction(action);
    },
    
    handleTreeSelection : function(component, event, helper) {
        var selection = event.getParam("selection");
        console.log("Tree selection: "+ JSON.stringify(selection.ID));
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            recordId: selection.ID
        });
        navEvt.fire();
    }
})