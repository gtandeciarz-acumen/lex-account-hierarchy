/**
 * Created by gtandeciarz on 6/25/17.
 */

global virtual class AccountHierarchy {

    public static Id SelectedAccountID{ get; set; }
    Account a;
    public AccountHierarchy(){

    }
    public AccountHierarchy(Account pa){
        this.a = pa;
    }
    public AccountHierarchy addAccount(Account act){
        AccountHierarchy ah = new AccountHierarchy(act);
        return this.addNode(ah);
    }
    public AccountHierarchy addNode(AccountHierarchy node){
        //is this the first node?
        if(this.a == null){
            return node;
        }
        //is this an immediate child
        if(node.a.ParentId == this.a.Id){
            //check if already in children
            if(!this.childrenIds.contains(node.a.Id)){
                //this.childrenMap.put(node.a.Id, new List<AccountHierarchy>{node});
                this.childrenIds.add(node.a.Id);
                this.children.add(node);
            }
        }else if(this.a.ParentId != null && this.a.ParentId == node.a.Id){
            //new parent
            return node.addNode(this);
        }
        else{
            //loop through all children
            AccountHierarchy n = this.childWithKey(node.a.ParentId);
            if(n!= null){
                return n.addNode(node);
            }
        }
        return this;
    }
    @AuraEnabled
    public String Name {get {return a.Name;}}
    @AuraEnabled
    public String ID {get {return a.Id;}}
    @AuraEnabled
    public Boolean CurrentAccount { get {
        return AccountHierarchy.SelectedAccountID == this.a.Id;
    }}
    @AuraEnabled
    public List<AccountHierarchy> children = new List<AccountHierarchy>();
    @testVisible
    AccountHierarchy get(Id aId){
        if(aId == a.Id){
            return this;
        }
        return this.childWithKey(aId);
    }
    AccountHierarchy childWithKey(Id aId){
        for(AccountHierarchy ah : children){
            if(ah.a.Id == aId){
                return ah;
            }
            if(ah.childWithKey(aId)!=null){
                return ah.childWithKey(aId);
            }
        }
        return null;
    }
    Set<Id> childrenIds = new Set<Id>();
    //Map<Id, List<AccountHierarchy>> childrenMap = new Map<Id,List<AccountHierarchy>>();

    public static Id GetRootAccountID(Id objId){
        Boolean top = false;
        Account a;
        while ( !top ) {
            //Change below
            a = [ Select a.Id, a.ParentId, a.Name From Account a where a.Id =: objId limit 1 ];

            if ( a.ParentID != null ) {
                objId = a.ParentID;

            }
            else {
                top = true;
            }
        }
        return objId ;
    }

}