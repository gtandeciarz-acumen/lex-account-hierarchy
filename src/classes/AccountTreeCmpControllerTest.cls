/**
 * Created by gtandeciarz on 6/25/17.
 */
 
@IsTest
private class AccountTreeCmpControllerTest {

    @testSetup
    static void setup(){
        Account parent = new Account(Name='Parent Account');
        insert parent;
        Account child1 = new Account(Name='Child1 with children', ParentId=parent.Id);
        Account child2 = new Account(Name='Child2 without children', ParentId=parent.Id);
        Account child3 = new Account(Name='Child3 with children', ParentId=parent.Id);
        insert new Account[]{child1,child2,child3};
        Account child1child1 = new Account(Name='Child1 Child 1', ParentId=child1.Id);
        Account child1child2 = new Account(Name='Child1 Child 2', ParentId=child1.Id);
        Account child1child3 = new Account(Name='Child1 Child 3', ParentId=child1.Id);
        Account child3child1 = new Account(Name='Child3 Child 1', ParentId=child3.Id);
        Account child3child2 = new Account(Name='Child3 Child 2', ParentId=child3.Id);
        Account child3child3 = new Account(Name='Child3 Child 3', ParentId=child3.Id);
        insert new Account[]{child1child1,child1child2,child1child3,child3child1,child3child2,child3child3};
        insert new Account(Name='Child1 Child1 child', ParentId=child1child1.Id);

    }
    static testMethod void testStructure() {
        Map<Id,Account> allAccounts = new Map<Id,Account>( [SELECT Id, ParentId, Name FROM Account] );
        Map<Id,Account> accountsWithChildren = new Map<Id, Account>();
        Map<Id,List<Account>> childrenWithParentsByParentId = new Map<Id, List<Account>>();
        for(Account a : allAccounts.values()){
            if(a.ParentId != null){
                Account p = allAccounts.get(a.ParentId);
                accountsWithChildren.put(p.Id, p);
                if(childrenWithParentsByParentId.containsKey(p.Id)){
                    childrenWithParentsByParentId.get(p.Id).add(a);
                }else{
                    childrenWithParentsByParentId.put(p.Id, new Account[]{a});
                }
            }
        }
        test.startTest();

        System.assert(allAccounts.size() > 0);
        System.assert(accountsWithChildren.size() > 0);
        System.assert(childrenWithParentsByParentId.size() > 0);
        System.assertEquals(childrenWithParentsByParentId.keySet().size(), accountsWithChildren.keySet().size());
        Account parent = [SELECT Id, ParentId from Account where ParentId = null LIMIT 1];
        System.assert(parent.ParentId == null);
        Account childAccount = [SELECT Id, Name, ParentId FROM Account where ParentId = :parent.Id Limit 1];

        AccountTreeCmpController controller = new AccountTreeCmpController();
        AccountTreeCmpController.accountId = childAccount.Id;
        Id top = AccountHierarchy.GetRootAccountId(childAccount.Id);
        System.assertEquals(top, parent.Id);
        AccountTreeCmpController.accountId = null;
        AccountHierarchy hierarchy = AccountTreeCmpController.getHierarchyData(childAccount.Id);
        System.debug(JSON.serializePretty(hierarchy));
        System.assertEquals(hierarchy.get(childAccount.Id).ID , childAccount.Id);
        AccountHierarchy root = hierarchy.addAccount(parent);
        Account rootParent = new Account(Name='Root parent', ParentId=top);
        system.debug(JSON.serializePretty(root));
        test.stopTest();
        AccountHierarchy empty = new AccountHierarchy().addAccount(childAccount);
        empty.addAccount(parent);
        system.debug(empty);

    }
}