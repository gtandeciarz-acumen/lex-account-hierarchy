global class AccountTreeCmpController {
    @testVisible
    static String accountId;
	@AuraEnabled
    global static AccountHierarchy getHierarchyData(Id accountId) {
        AccountTreeCmpController.accountId = accountId;
        AccountHierarchy.SelectedAccountID = accountId;
        System.debug('Account ID:' + accountId);
        Account thisAccount = [SELECT name, parentId From Account where Id =:accountId];
        ObjectStructure obj = new ObjectStructure();
        AccountHierarchy hierarchy = obj.BuildStructure(accountId);
        System.debug(hierarchy);
        return hierarchy;
    }

    global class ObjectStructure{
        AccountHierarchy hierarchy;
        public AccountHierarchy BuildStructure(string currentId){

            List<Account> al            = new List<Account>{};
            List<ID> currentParent      = new List<ID>{};
            Integer count               = 0;
            Integer level               = 0;
            Boolean endOfStructure      = false;

            //Find highest level obejct in the structure
            Id rootId = AccountHierarchy.GetRootAccountID(currentId);
            currentParent.add( rootId );

            //Loop though all children
            while ( !endOfStructure ){

                if( level == 0 ){
                    al = [ SELECT a.Type, a.Site, a.ParentId, a.OwnerId, a.Name, a.Industry, a.Id FROM Account a WHERE a.id IN : CurrentParent ORDER BY a.Name ];
                }
                else {
                    al = [ SELECT a.Type, a.Site, a.ParentId, a.OwnerId, a.Name, a.Industry, a.Id FROM Account a WHERE a.ParentID IN : CurrentParent ORDER BY a.Name ];
                }

                if( al.size() == 0 ){
                    endOfStructure = true;
                }
                else{
                    currentParent.clear();
                    for ( Integer i = 0 ; i < al.size(); i++ ){
                        Account a = al[i];
                        AccountHierarchy ah = new AccountHierarchy(a);
                        if(this.hierarchy == null){
                            this.hierarchy = ah;
                        }
                        else{
                            this.hierarchy.addNode(ah);
                        }
                        currentParent.add( a.id );
                    }
                    level++;
                }
            }
            return this.hierarchy;
        }

    }

}